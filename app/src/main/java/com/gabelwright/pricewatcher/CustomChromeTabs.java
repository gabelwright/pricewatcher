package com.gabelwright.pricewatcher;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import androidx.browser.customtabs.CustomTabsIntent;

import static android.graphics.BitmapFactory.decodeResource;

public class CustomChromeTabs extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String url = intent.getDataString();

        if (url != null) {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_TEXT, url);

            Intent chooserIntent = Intent.createChooser(shareIntent, "Share url");
            chooserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(chooserIntent);
        }
    }

    public static void launchTab(Context context, String url){
        Log.i("mgw", "website launched");
        Bitmap icon = decodeResource(context.getResources(), R.drawable.share_icon);

        Intent actionIntent = new Intent(context, CustomChromeTabs.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, actionIntent, 0);

        try{
            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
            builder.setActionButton(icon, "Add URL to watch list", pendingIntent, false);
            CustomTabsIntent customTabsIntent = builder.build();
            customTabsIntent.launchUrl(context, Uri.parse(url));
        }catch(Exception e){
            Toast.makeText(context, context.getString(R.string.bad_url_error), Toast.LENGTH_LONG).show();
        }
    }
}
