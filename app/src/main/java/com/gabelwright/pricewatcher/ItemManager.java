package com.gabelwright.pricewatcher;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class ItemManager{

    private static final String ITEMS_LOCATION = "items";

    /** List of items */
    private List<Item> items;
    private ChangeListener listener;
    private SortMethod sortMethod;

    /** Constructor */
    ItemManager() {
        items = new ArrayList<>();
        sortMethod = SortMethod.DATE;

        //Gets reference to the firebase database
        DatabaseReference ref = getRef(ITEMS_LOCATION);

        //Grabs data from database
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //Removes old items from list
                items.clear();
                //Repopulates list with fresh items from database
                for(DataSnapshot data : dataSnapshot.getChildren()){
                    Item new_item = createItemFromFB(data);
                    items.add(new_item);
                }
                //Sorts items as needed
                if(sortMethod == SortMethod.ALPHA)
                    sortByAlpha();
                else if(sortMethod == SortMethod.PRICE)
                    sortByPrice();
                //Notifies activity of a change in data
                if (listener != null) {
                    listener.onChangeHappened();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Failed to read value
                Log.i("mgw", "Failed to read value.", error.toException());
            }
        });
    }

    //Sets the event listener
    void setChangeListener(ChangeListener listener) {
        this.listener = listener;
    }

    /** Returns the number of items in list */
    public int count() {
        return items.size();
    }

    /** Adds new item to list */
    static void addItem(Item item) {
        DatabaseReference ref = getRef(ITEMS_LOCATION);
        String key = ref.push().getKey();
        item.setId(key);
        assert key != null;
        ref.child(key).setValue(item);
    }

    /** Updates the prices of all items */
    void refreshAll(){
        for(int i=0; i<items.size(); i++){
            int finalI = i;
            new Thread(() -> {
                Item item = items.get(finalI);
                boolean found = item.updatePrice();
                if(found)
                    saveItem(item);
            }).start();
        }
    }

    static void saveItem(Item item){
        DatabaseReference ref = getRef(ITEMS_LOCATION + "/" + item.getId());
        ref.setValue(item);
    }

    /** Deletes item from list */
    void deleteItem(int position) {
        String id = this.getItem(position).getId();
        DatabaseReference ref = getRef(ITEMS_LOCATION + "/" + id);
        ref.removeValue();
    }

    /** Helper method that parses Firebase data into an item object */
    private static Item createItemFromFB(DataSnapshot data){
        String name = Objects.requireNonNull(data.child("name").getValue()).toString();
        String url = Objects.requireNonNull(data.child("url").getValue()).toString();
        int originalPrice = (int)(long)data.child("originalPrice").getValue();
        int currentPrice = (int)(long) data.child("currentPrice").getValue();
        long seconds = (long) data.child("added").child("time").getValue();
        Date date = new Date(seconds);
        String id = data.getKey();

        return new Item(name, url, originalPrice, currentPrice, date, id);
    }

    /** Getter method for items */
    List<Item> getAllItems(){
        return items;
    }

    /** Sets up Change listener for the items list */
    public interface ChangeListener {
        void onChangeHappened();
    }

    /** Helper method that returns a db reference to Firebase */
    private static DatabaseReference getRef(String ref){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        return database.getReference(ref);
    }

    /** Returns item at given geekstore */
    Item getItem(int i){
        return items.get(i);
    }

    /** Sorts list by price */
    void sortByPrice(){
        Collections.sort(items, new SortByPrice());
        sortMethod = SortMethod.PRICE;
        if (listener != null) {
            listener.onChangeHappened();
        }
    }

    /** Sorts list by date */
    void sortByDate(){
        Collections.sort(items, new SortByDate());
        sortMethod = SortMethod.DATE;
        if (listener != null) {
            listener.onChangeHappened();
        }
    }

    /** Sorts list by alpha */
    void sortByAlpha(){
        Collections.sort(items, new SortByAlpha());
        sortMethod = SortMethod.ALPHA;
        if (listener != null) {
            listener.onChangeHappened();
        }
    }

    private void log(String m){
        Log.i("mgw", m);
    }
}

enum SortMethod{
    DATE, ALPHA, PRICE
}
