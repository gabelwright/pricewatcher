package com.gabelwright.pricewatcher;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceManager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class MainActivity extends AppCompatActivity implements ItemManager.ChangeListener {

    ItemManager itemManager;
    ItemListAdaptor itemListAdaptor;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Creates listview and populates it from database
        listView = findViewById(R.id.item_list_view);

        itemManager = new ItemManager();
        itemManager.setChangeListener(this);

        itemListAdaptor = new ItemListAdaptor(this, itemManager.getAllItems());
        listView.setAdapter(itemListAdaptor);

        //Loads the detailed item activity when an item is clicked
        listView.setOnItemClickListener((AdapterView<?> adapterView, View view, int i, long l) -> {
            Item item = itemManager.getItem(i);
            Intent intent = new Intent(getApplicationContext(), ItemActivity.class);
            intent.putExtra("item", item);
            startActivity(intent);
        });

        //Registers the popup menu to the list view items
        registerForContextMenu(listView);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        set_prefs(sharedPref);

        //Loads the new item activity when the floating plus sign is clicked
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), NewItemActivity.class);
            startActivity(intent);
        });

        if(!isNetworkAvailable()){
            Toast.makeText(getApplicationContext(), "Please turn on wifi/network connection to continue", Toast.LENGTH_LONG).show();
            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
        }
    }

    private void set_prefs(SharedPreferences sp){
        boolean dark_mode = sp.getBoolean(getResources().getString(R.string.dark_theme_key), false);
        String sort_method = sp.getString(getResources().getString(R.string.sorting_key), "date");

        if(dark_mode)
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        else
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        switch (sort_method){
            case "date":
                itemManager.sortByDate();
                break;
            case "price":
                itemManager.sortByPrice();
                break;
            case "alpha":
                itemManager.sortByAlpha();
                break;
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    /**
     * Creates the dropdown menu in the activity bar
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.item_list_menu, menu);
        return true;
    }

    /**
     * Controls what happens when a menu item is selected
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.item_list_menu_update_all) {
            itemManager.refreshAll();
            return true;
        }
        else if(id == R.id.item_list_menu_sort_by_date){
            itemManager.sortByDate();
        }
        else if(id == R.id.item_list_menu_sort_by_alpha){
            itemManager.sortByAlpha();
        }
        else if(id == R.id.item_list_menu_sort_by_price){
            itemManager.sortByPrice();
        }
        else if(id == R.id.store_geek_store){
            CustomChromeTabs.launchTab(this, "https://us.geekstore.com/");
        }
        else if(id == R.id.store_walmart){
            CustomChromeTabs.launchTab(this, "https://www.walmart.com");
        }
        else if(id == R.id.item_menu_settings){
            Intent i = new Intent(this, SettingsActivity.class);
            startActivityForResult(i, 1);
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Updates the listview data whenever there is a change to the item manager data
     */
    @Override
    public void onChangeHappened() {
        itemListAdaptor.notifyDataSetChanged();
    }

    /**
     * Creates a popup menu when an item is long-pressed
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.item_list_view_menu, menu);
    }

    /**
     * Controls what happens when a menu item is selected
     */
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int id = item.getItemId();
        int position = (int)info.id;

        if(id == R.id.item_list_view_update_price){
            new Thread(() -> {
                boolean found = itemManager.getItem(position).updatePrice();
                runOnUiThread(() -> {
                    if(found){
                        ItemManager.saveItem(itemManager.getItem(position));
                        Toast.makeText(getApplicationContext(), "Price updated", Toast.LENGTH_LONG).show();
                    }
                    else
                        Toast.makeText(getApplicationContext(), "Cannot find current price. Please check the URL and try again.", Toast.LENGTH_LONG).show();
                });
            }).start();
        }
        else if(id == R.id.item_list_view_edit){
            Intent intent = new Intent(getApplicationContext(), NewItemActivity.class);
            intent.putExtra("edit_item", itemManager.getItem(position));
            startActivity(intent);
        }
        else if(id == R.id.item_list_view_delete){
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle(getString(R.string.delete));
            builder.setMessage(getString(R.string.delete_confirmation));

            builder.setPositiveButton(getString(R.string.delete), (dialogInterface, i) -> {
                itemManager.deleteItem(position);
                Toast.makeText(getApplicationContext(), R.string.item_deleted, Toast.LENGTH_LONG).show();
            });
            builder.setNegativeButton(getString(R.string.cancel), null);
            builder.show();
        }
        return true;
    }

    /**
     * ArrayAdaptor class that creates the item view rows
     */
    private static class ItemListAdaptor extends ArrayAdapter<Item>{

        private final List<Item> items;

        ItemListAdaptor(Context context, List<Item> items){
            super(context, -1, items);
            this.items = items;
        }

        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent){
            View row = convertView != null ? convertView
                    : LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_list_row, parent, false);
            TextView name = row.findViewById(R.id.item_list_item_name);
            TextView price = row.findViewById(R.id.item_list_item_price);
            TextView percent = row.findViewById(R.id.item_list_percent_change);
            ImageView icon = row.findViewById(R.id.item_list_store_icon);

            Item item = items.get(position);
            name.setText(item.getName());
            price.setText(item.currentPrice());
            icon.setImageResource(item.icon());

            String percentChange = item.percentChange() + "% change";
            percent.setText(percentChange );

            if(percentChange.startsWith("-"))
                percent.setTextColor(Color.GREEN);
            else if(!percentChange.startsWith("0%"))
                percent.setTextColor(Color.RED);

            return row;
        }
    }

    private void log(String m){
        Log.i("mgw", m);
    }

}
