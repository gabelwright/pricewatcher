package com.gabelwright.pricewatcher;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.GZIPInputStream;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class WebChecker{

    private Item item;

    public WebChecker(Item item) {
        this.item = item;
    }

    public int getCurrentPrice() {
        log("starting to scrap");
        BufferedReader html = scrap(item.getUrl());

        if(html == null)
            return -1;

        String p = null;
        if(item.getUrl().contains("walmart")){
            p = parseWalmart(html);
        }else if(item.getUrl().contains("geekstore")){
            p = parseGeekStore(html);
        }
        if(p != null){
//            item.setCurrentPrice(parsePrice(p));
//            ItemManager.saveItem(item);
            log("done scraping");
            return parsePrice(p);
        }
        else{
            return -1;
        }
    }

    /** Fetches the html of the given url */
    private static BufferedReader scrap(String webURL){

        HttpURLConnection con;
        try {
            URL url = new URL(webURL);
            con = (HttpURLConnection) url.openConnection();
            if(!webURL.contains("lowes"))
                con.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:63.0) Gecko/20100101 Firefox/63.0");
            String encoding = con.getContentEncoding();
            if (encoding == null) { encoding = "utf-8"; }
            InputStreamReader reader;

            if ("gzip".equals(encoding)) { // gzipped document?
                reader = new InputStreamReader(new GZIPInputStream(con.getInputStream()));
            } else {
                reader = new InputStreamReader(con.getInputStream(), encoding);
            }
            BufferedReader in = new BufferedReader(reader);
            return in;

        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String parseWalmart(BufferedReader html){
        Document doc;
        String line;
        try{
            while((line = html.readLine()) != null){
                doc = Jsoup.parse(line);
                Element item = doc.getElementsByClass("price-characteristic").first();
                if(item != null){
                    html.close();
                    return item.attr("content");
                }

            }
        }
        catch (Exception e){
            log(e.toString());
        }
        return null;
    }

    private static String parseGeekStore(BufferedReader html){
        Document doc;
        String line;
        try{
            while((line = html.readLine()) != null){
                doc = Jsoup.parse(line);
                Element item = doc.getElementById("product-price");
                if(item != null){
                    html.close();
                    log("text: " + item.text());
                    return item.text();
                }
            }
        }
        catch (Exception e){
            log(e.toString());
        }
        return null;
    }

    /** Converts the string value price into an int */
    private static int parsePrice(String price){
        price = price.replace("$","").replace(".","");
        try{
            int i = Integer.parseInt(price);
            return i;
        }catch(Exception e){
            System.out.println(e.toString());
        }
        return -1;
    }

    public static boolean isSupportedStore(String url){
        if(!android.util.Patterns.WEB_URL.matcher(url).matches())
            return false;
        if(url.contains("geekstore") || url.contains("walmart"))
            return true;
        return false;
    }

    private static void log(String m){
        Log.i("mgw", m);
    }
}
