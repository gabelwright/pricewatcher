package com.gabelwright.pricewatcher;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ItemActivity extends AppCompatActivity implements Item.ChangeListener{

    private Item item;
    private TextView itemName;
    private TextView initalPrice;
    private TextView currentPrice;
    private TextView percentChange;
    private Button updatePriceButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);

        item = (Item)getIntent().getSerializableExtra("item");
        item.setChangeListener(this);

        itemName = findViewById(R.id.ItemView_ItemName);
        initalPrice = findViewById(R.id.ItemView_initialPrice);
        currentPrice = findViewById(R.id.ItemView_currentPrice);
        percentChange = findViewById(R.id.ItemView_percentChange);
        updatePriceButton = findViewById(R.id.item_update_price_button);

        updateUI();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.title_activity_item_list));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.item_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();

        if(id == 16908332){
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(menuItem);
    }

    private void updateUI(){
        itemName.setText(item.getName());
        initalPrice.setText(item.originalPrice());
        currentPrice.setText((item.currentPrice()));
        percentChange.setText(item.percentChange() + "%");
        updatePriceButton.setEnabled(true);
    }

    public void updateItem(View view){
        updatePriceButton.setEnabled(false);
        new Thread(() -> {
            boolean found = item.updatePrice();
            runOnUiThread(()-> {
                if(found){
                    ItemManager.saveItem(item);
                    updateUI();
                    Toast.makeText(getApplicationContext(), "Price Updated", Toast.LENGTH_LONG).show();
                }
                else
                    Toast.makeText(getApplicationContext(), "Cannot find current price. Please check the URL and try again.", Toast.LENGTH_LONG).show();
            });
        }).start();

    }

    public void visitWebsite(View view){
        CustomChromeTabs.launchTab(this, item.getUrl());
    }

    @Override
    public void onChangeHappened() {
        log("on change listener called");
        updateUI();
    }

    void log(String m){
        Log.i("mgw", m);
    }
}
