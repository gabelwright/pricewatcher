package com.gabelwright.pricewatcher;

import android.util.Log;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

public class Item implements Serializable {

    //Item properties
    private String url;
    private String name;
    private int originalPrice;  // Money values are stored in cents as integers to prevent rounding issues
    private int currentPrice;   // Money values are stored in cents as integers to prevent rounding issues
    private float percentChange;
    private Date added;
    private String id;
    private ChangeListener listener;

    /**
     * Creates new item object
     * @param _name item getName
     * @param _url item getUrl
     */
    Item(String _name, String _url){
        url = _url;
        name = _name;
        originalPrice = 1;
        currentPrice = originalPrice;
        percentChange = (float)0.0;
        added = new Date();
    }

    Item(String name, String url, int originalPrice, int currentPrice, Date added, String id) {
        this.url = url;
        this.name = name;
        this.originalPrice = originalPrice;
        this.currentPrice = currentPrice;
        this.percentChange = 0;
        this.added = added;
        this.id = id;

        calcPercentChange();
    }

    /**
     * This constructor is required to interface with Firebase properly
     */
    public Item(){
        this("unknown", "www.amazon.com");
    }

    public void setName(String name){
        this.name = name;
    }

    //Sets the event listener
    void setChangeListener(ChangeListener listener) {
        this.listener = listener;
    }

    /**
     * These getters and setters are required to interface with Firebase properly
     */
    public int getOriginalPrice(){
        return originalPrice;
    }

    public int getCurrentPrice(){
        return currentPrice;
    }

    public float getPercentChange(){
        return percentChange;
    }

    public Date getAdded(){
        return added;
    }

    public String getId(){
        return id;
    }

    public String getUrl() {
        return url;
    }

    void setUrl(String _url) {
        this.url = _url;
    }

    /**
     * Returns the item getName
     * @return getName of the item
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the original price of the item as a readable string
     * @return the original price as string
     */
    String originalPrice() {
        if(originalPrice == 0)
            return "";
        String price = Integer.toString(originalPrice);
        price = price.substring(0, price.length()-2) + "." + price.substring(price.length()-2);
        return "$" + price;
    }

    /**
     * Returns the current price of the item as a readable string
     * @return the current price as a string
     */
    String currentPrice() {
        if(currentPrice == 0)
            return "";
        String price = Integer.toString(currentPrice);
        price = price.substring(0, price.length()-2) + "." + price.substring(price.length()-2);
        return "$" + price;
    }

    void setCurrentPrice(int price){
        currentPrice = price;
        calcPercentChange();
    }

    public void setOriginalPrice(int price){
        originalPrice = price;
        calcPercentChange();
    }

    public void setId(String _id){
        id = _id;
    }

    /**
     * Returns the percent change of the item
     * @return the percent change as a string
     */
    String percentChange(){
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        return df.format(percentChange);
    }

    /**
     * Calculates and updates the percent change in price of an item
     */
    private void calcPercentChange(){
        if(currentPrice == originalPrice || originalPrice == 0)
            percentChange = 0;
        else
            percentChange = (((float)currentPrice - (float)originalPrice) / (float)originalPrice) * 100;
    }

    /**
     * Fetches the updated price of this item and updates the percent change.
     */
    boolean updatePrice(){
        WebChecker webChecker = new WebChecker(this);
        int price = webChecker.getCurrentPrice();
        if(price != -1){
            this.setCurrentPrice(price);
            return true;
        }
        return false;
    }

    /**
     * Takes in the Java Date object and returns a readable string version of it
     * @return a readable string version of the item's date object
     */
    public String addedAsString() {
        SimpleDateFormat ft = new SimpleDateFormat("M/d/y", Locale.US);
//        SimpleDateFormat ft = new SimpleDateFormat ("M/d/y");
        return ft.format(added);
    }

    /** Gets the proper icon based on its getUrl */
    int icon(){
        if(url.contains("geekstore"))
            return R.drawable.geekstore;
        else if(url.contains("bestbuy"))
            return R.drawable.bestbuy;
        else if(url.contains("walmart"))
            return R.drawable.walmart;
        else if(url.contains("utep"))
            return R.drawable.cheon;
        else
            return R.mipmap.ic_launcher_round;
    }

    @NonNull
    public String toString(){
        return name + ": " + url;
    }

    /** Sets up Change listener for the items list */
    public interface ChangeListener {
        void onChangeHappened();
    }

    private static void log(String m){
        Log.i("mgw", m);
    }
}

/** Sorts items by price */
class SortByPrice implements Comparator<Item> {

    @Override
    public int compare(Item o1, Item o2) {
        return o1.getCurrentPrice() - o2.getCurrentPrice();
    }
}

/** Sorts items by date */
class SortByDate implements Comparator<Item> {

    @Override
    public int compare(Item o1, Item o2) {
        return (int) (o1.getAdded().getTime() - o2.getAdded().getTime());
    }
}

/** Sorts items by alpha */
class SortByAlpha implements Comparator<Item> {

    @Override
    public int compare(Item o1, Item o2) {
        return o1.getName().compareTo(o2.getName());
    }
}

