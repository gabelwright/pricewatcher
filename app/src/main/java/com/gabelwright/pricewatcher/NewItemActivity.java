package com.gabelwright.pricewatcher;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class NewItemActivity extends AppCompatActivity {

    Item item;
    EditText nameF;
    EditText urlF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_item);

        nameF = findViewById(R.id.new_item_name);
        urlF = findViewById(R.id.new_item_url);

        //Checks if item was sent from previous activity. If it was, it edits it,
        //otherwise it creates a new item
        item = (Item)getIntent().getSerializableExtra("edit_item");

        if(item != null){
            TextView title = findViewById(R.id.new_item_title);
            title.setText(getString(R.string.edit_text));
            nameF.setText(item.getName());
            urlF.setText(item.getUrl());
        }

        //Listens for intent from other apps sharing text
        Intent shareTextIntent = getIntent();
        String action = shareTextIntent.getAction();
        String type = shareTextIntent.getType();

        //If text is shared, it populates it into url field
        if (Intent.ACTION_SEND.equals(action) && type != null) {
            Log.i("mgw", "sent intent");
            if ("text/plain".equals(type)) {
                String sharedText = shareTextIntent.getStringExtra(Intent.EXTRA_TEXT);
                urlF.setText(sharedText);
            }
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.title_activity_item_list));
    }

    /**
     * onClick method that creates/updates item when save button is clicked
     */
    public void saveItem(View view){
        String name = nameF.getText().toString();
        String url = urlF.getText().toString();

        //Validates name and url
        if(name.isEmpty())
            Toast.makeText(getApplicationContext(), "Please provide a valid name", Toast.LENGTH_LONG).show();
        else if(!WebChecker.isSupportedStore(url))
            Toast.makeText(getApplicationContext(), "Please provide a valid URL for GeekStore or Walmart", Toast.LENGTH_LONG).show();
        else{
            //If name and url is valid, creates or updates item
            if(item == null){
                Item new_item = new Item(name, url);

                new Thread(() -> {
                    boolean found = new_item.updatePrice();
                    log("found: " + found);
                    if(found){
                        new_item.setOriginalPrice(new_item.getCurrentPrice());
                        ItemManager.addItem(new_item);
//                        ItemManager.saveItem(new_item);
                    }
                    else{
                        runOnUiThread(()-> Toast.makeText(getApplicationContext(), "Cannot find current price. Please check the URL and try again.", Toast.LENGTH_LONG).show());
                    }
                }).start();
            }else{
                log("editing item");
                item.setName(name);
                item.setUrl(url);
                ItemManager.saveItem(item);
            }

            //Returns to launch activity
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();

        if(id == 16908332){
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(menuItem);
    }

    private static void log(String m){
        Log.i("mgw", m);
    }
}
